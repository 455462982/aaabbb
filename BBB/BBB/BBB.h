//
//  BBB.h
//  BBB
//
//  Created by tengpan on 2017/8/4.
//  Copyright © 2017年 tengpan. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BBB.
FOUNDATION_EXPORT double BBBVersionNumber;

//! Project version string for BBB.
FOUNDATION_EXPORT const unsigned char BBBVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BBB/PublicHeader.h>

#import <BBB/BBBManager.h>
