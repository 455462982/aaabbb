#!/bin/sh

FRAMEWORKS_DIR="../Carthage/Build/iOS"

mkdir -p "${FRAMEWORKS_DIR}"

checkEnvironment() {

echo "🛃 检查Carthage是否可用"
if ! [ -x "$(command -v carthage)" ]; then
echo '❌ 请先安装 Carthage' >&2
exit 1
else
echo "✅ Carthage检查通过" >&2
fi

}

getFileList() {
filelist=$(ls "$1")
while read line
do
eval "$2[\${#$2[*]}]=\"\$line\""
done <<< "$filelist"
}

build() {

local PROJECT_PATH=$1
local PROJECT_NAME=$2
local CARTHAGE_BUILD_DIR="./Carthage/Build/iOS"

pushd "${PROJECT_PATH}"

carthage build "${BBB_PROJECT_NAME}" --no-skip-current --platform iOS


echo "♻️ 遍历Carthage构建目录中 *.framework"

declare -a UF_FILE_LIST
getFileList "${CARTHAGE_BUILD_DIR}" UF_FILE_LIST
for file_name in "${UF_FILE_LIST[@]}"
do
#if [[ "${file_name}" =~ .*\.framework$ ]]
#then
cp -R "${CARTHAGE_BUILD_DIR}/${file_name}" "${FRAMEWORKS_DIR}"

#fi
done

## 清理
rm -rf "./Carthage"

popd

}

checkEnvironment

build "../BBB" "BBB"
build "../AAA" "AAA"


exit 0
