//
//  ViewController.m
//  Demo
//
//  Created by tengpan on 2017/8/4.
//  Copyright © 2017年 tengpan. All rights reserved.
//

#import "ViewController.h"
#import <AAA/AAA.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [[[AAAManager alloc] init] test];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
