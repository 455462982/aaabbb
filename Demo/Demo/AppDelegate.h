//
//  AppDelegate.h
//  Demo
//
//  Created by tengpan on 2017/8/4.
//  Copyright © 2017年 tengpan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

