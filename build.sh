#!/bin/sh

DEPENDENCY_FRAMEWORK_LIST=$@
FRAMEWORKS_DIR="../Carthage/Build/iOS"

#for dependency_framework_name in $DEPENDENCY_FRAMEWORK_LIST
#do
#echo "$dependency_framework_name"
#done

checkEnvironment() {

echo "🛃 检查Carthage是否可用"
if ! [ -x "$(command -v carthage)" ]; then
echo "❌ 请先安装 Carthage" >&2
exit 1
else
echo "✅ Carthage检查通过" >&2
fi

if [ ! -d "$CARTHAGE_BUILD_DIR/${DEPENDENCY_FRAMEWORK_NAME}.framework" ]; then
echo "📝创建Frmameworks目录：${FRAMEWORKS_DIR}" >&2
mkdir -p "${FRAMEWORKS_DIR}"
fi

}

getFileList() {
filelist=$(ls "$1")
while read line
do
eval "$2[\${#$2[*]}]=\"\$line\""
done <<< "$filelist"
}

clean() {
rm -rf $1
}

copy() {

local CARTHAGE_BUILD_DIR=$1
echo "♻️ 遍历${CARTHAGE_BUILD_DIR}目录" >&2

declare -a UF_FILE_LIST
getFileList ${CARTHAGE_BUILD_DIR} UF_FILE_LIST
for file_name in "${UF_FILE_LIST[@]}"
do
#if [[ "${file_name}" =~ .*\.framework$ ]]
#then
echo "📝 拷贝${file_name}至Frmameworks目录" >&2
cp -R ${CARTHAGE_BUILD_DIR}/${file_name} ${FRAMEWORKS_DIR}

#fi
done
}

build() {

local PROJECT_PATH=$1
local PROJECT_NAME=$2

if [ ! -d "$PROJECT_PATH" ]; then
echo "❌ 构建项目目录不存在：$PROJECT_PATH" >&2
exit 1
fi

local CARTHAGE_DIR="${PROJECT_PATH}/Carthage"
local CARTHAGE_BUILD_DIR="${CARTHAGE_DIR}/Build/iOS"

pushd "${PROJECT_PATH}"

carthage build "${PROJECT_NAME}" --no-skip-current --platform iOS

copy $CARTHAGE_BUILD_DIR

echo "🚧 清理临时文件" >&2
clean $CARTHAGE_DIR

popd

}

checkEnvironment

for dependency_framework_name in $DEPENDENCY_FRAMEWORK_LIST
do
echo "🛠 开始构建${dependency_framework_name}.framework" >&2
build ../$dependency_framework_name $dependency_framework_name
done

echo "🍺🍺🍺 构建完毕" >&2
exit 0
